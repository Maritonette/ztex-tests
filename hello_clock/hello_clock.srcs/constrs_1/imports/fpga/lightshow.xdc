# CLKOUT/FXCLK 
create_clock -name fxclk -period 10 [get_ports fxclk]
set_property PACKAGE_PIN P15 [get_ports fxclk]
set_property IOSTANDARD LVCMOS33 [get_ports fxclk]

# led1
set_property PACKAGE_PIN H15 [get_ports {led1[0]}]		;# A6 / B21~IO_L21P_T3_DQS_16
set_property PACKAGE_PIN J13 [get_ports {led1[1]}]		;# B6 / A21~IO_L21N_T3_DQS_16
set_property PACKAGE_PIN J14 [get_ports {led1[2]}]		;# A7 / D20~IO_L19P_T3_16
set_property PACKAGE_PIN H14 [get_ports {led1[3]}]		;# B7 / C20~IO_L19N_T3_VREF_16
set_property PACKAGE_PIN H17 [get_ports {led1[4]}]		;# A8 / B20~IO_L16P_T2_16
set_property PACKAGE_PIN G14 [get_ports {led1[5]}]		;# B8 / A20~IO_L16N_T2_16
set_property PACKAGE_PIN G17 [get_ports {led1[6]}]		;# A9 / C19~IO_L13N_T2_MRCC_16
set_property PACKAGE_PIN G16 [get_ports {led1[7]}]		;# B9 / A19~IO_L17N_T2_16
set_property PACKAGE_PIN G18 [get_ports {led1[8]}]		;# A10 / C18~IO_L13P_T2_MRCC_16
set_property PACKAGE_PIN H16 [get_ports {led1[9]}]		;# B10 / A18~IO_L17P_T2_16
set_property IOSTANDARD LVCMOS33 [get_ports {led1[*]}]
set_property DRIVE 12 [get_ports {led1[*]}]
#set_property DRIVE 12 [get_ports {B}]

# sw
#set_property PACKAGE_PIN F18 [get_ports {A}]		;# A11 / B18~IO_L11N_T1_SRCC_16
#set_property PACKAGE_PIN F16 [get_ports {sw[1]}]		;# B11 / D17~IO_L12P_T1_MRCC_16
#set_property PACKAGE_PIN E18 [get_ports {sw[2]}]		;# A12 / B17~IO_L11P_T1_SRCC_16
#set_property PACKAGE_PIN F15 [get_ports {sw[3]}]		;# B12 / C17~IO_L12N_T1_MRCC_16
#set_property IOSTANDARD LVCMOS33 [get_ports {sw[*]}]
#set_property PULLUP true [get_ports {A}]


# bitstream settings
#set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]  
#set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR No [current_design]
#set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 2 [current_design]
#set_property BITSTREAM.GENERAL.COMPRESS true [current_design] 
