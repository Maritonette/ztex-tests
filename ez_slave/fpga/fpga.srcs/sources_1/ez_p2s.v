`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/15/2015 01:15:22 PM
// Design Name: 
// Module Name: ez_p2s
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//module ez_p2s(
//        input wire fxclk,
//        input wire [7:0] porta,
//        input wire [5:0] portc,
    
//        output [9:0] led1,
//        output [19:0] led2
//    );
    
//    assign led1[7:0] = porta;
    
//    wire SLOWCLOCK;
//    assign led1[9] = SLOWCLOCK;
    
//    slow_clock(
//        .FASTCLOCK(fxclk),
//        .SLOWCLOCK(SLOWCLOCK)
//    );
    
//    parallel_to_serial(
//        .PDATA(porta),
//        .LOAD(portc[0]),
        
//        .SDATA(led1[8]),
//        .CLOCK(SLOWCLOCK),
        
//        .debug(led2[19:0])
//    );
//endmodule


// slow_clock takes a clock signal and generates another 
// clock signal integer times slower than the first one.
module slow_clock(
        input wire FASTCLOCK,
        output reg SLOWCLOCK
    );

    parameter MAXCOUNT = 12000000;
    integer counter = 0;


    always @(posedge FASTCLOCK) begin
        if (counter < MAXCOUNT)
            counter <= counter + 1;
        else begin
            counter <= 0;
            SLOWCLOCK <= SLOWCLOCK + 1;
        end
    end
endmodule

// create a finite state machine to act as serial -> parallel converter.
module parallel_to_serial(
        input wire [7:0] PDATA,
        input wire LOAD,
        
        output wire SDATA,
        input wire CLOCK,
        
        output wire [19:0] debug
    );
    
    // register to store the current data to convert to serial
    reg [17:10] buffer = 8'b00110001;
    
    // use an integer to specify which bit we are on
    integer n = 0,
            next_n = 0;

    
    // use a finite state machine with 2 states, either
    localparam S_OUT = 2'b1, // writing serial data, or
               S_IDLE = 2'b0; // finished writing and doing nothing
    
    reg state = S_OUT,
        next_state = S_OUT;
    
    // use combinational logic to output the serial bit
    assign SDATA = (state == S_OUT)? buffer[n] : 0;
    
    //// debug LEDs
    assign debug[10] = LOAD;
    assign debug[18:11] = 0;
    assign debug[19] = state;
    //assign debug[8] = out;
                
    assign debug[7:0] = 1 << n;
    assign debug[8] = 0;
    assign debug[9] = SDATA;
    
    // combinational section for determining next state
    always @(state) begin
        // give default values to prevent "latch generation"
        next_state = state;
        next_n = n;

        case (state)
            S_OUT: begin
                if (n >= 7) // go to idle state if wrote all the bits
                    next_state = S_IDLE;
                else // otherwise increment bit number
                    next_n = n + 1;
            end
        endcase
    end
    
    //  Use and "asyncronous reset" to load new data as the serial out clock is
    //  much slower than load signals are switched
    always @(posedge CLOCK or posedge LOAD) begin
        if (LOAD) begin
            state <= S_OUT;
            buffer <= PDATA;
            n <= 0;
        end else begin
            state <= next_state;
            n <= next_n;        
        end
    end

endmodule
    
