/*!
   ucecho -- uppercase conversion and bitstream encryption example for ZTEX USB-FPGA Module 2.13
   Copyright (C) 2009-2014 ZTEX GmbH.
   http://www.ztex.de

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3 as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see http://www.gnu.org/licenses/.
!*/

#include[ztex-conf.h]	// Loads the configuration macros, see ztex-conf.h for the available macros
#include[ztex-utils.h]	// include basic functions

// configure endpoints 2 and 4, both belong to interface 0 (in/out are from the point of view of the host)
EP_CONFIG(2,0,BULK,IN,512,2);

// select ZTEX USB FPGA Module 1.16 as target (required for FPGA configuration)
IDENTITY_UFM_2_13(10.17.0.0,0);

// this product string is also used for identification by the host software
#define[PRODUCT_STRING]["poopmaster 5050"]

// enables high speed FPGA configuration via EP4
EP_CONFIG(4,0,BULK,OUT,512,2);
ENABLE_HS_FPGA_CONF(4);

ENABLE_FLASH;
ENABLE_FLASH_BITSTREAM;



// Use ZTEX macros to setup interrupt when a setup packet is sent to device
ADD_EP0_VENDOR_COMMAND((0x80,,
	    // SETUPDAT is 8 byte array containing the setup packet
		// set port A pins to the first "value" byte of the setup packet
	IOA = SETUPDAT[2];
	SYNCDELAY;
	IOC = 0x01;
	// SYNCDELAY;
	wait(5);
	IOC = 0x00;
,,
	NOP;
));;


// include the main part of the firmware kit, define the descriptors, ...
#include[ztex.h]

void main(void)
{
	// init everything
    init_USB();

	// Set port a to be outputs
	OEA = 0xff;
	IOA = 0b00101001;

	OEC = 0x3f;
	IOC = 0x0;


	while (1) {}
}
